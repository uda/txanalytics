# txAnalytics

Twisted based analytics client

## Usage

This is in alpha stage and implements only the parts necessary for our project,
so only Google Analytics Events are supported, more will come.

First install the package

```bash
pip install txanalytics
```

Then add your tracking to your code, remember this is intended to be a silent
step, so this example puts the tracking and then passing back the original
information passed to the track callback.

```python
from txanalytics.googleanalytics import Event

def track_cbk(data):
    deferred = Event(
        tracking_id='UA-xxxxx-y',
        client_id='a-uuid-4',
        category='pip',
        action='install',
        label=data.package_name
    ).track()
    deferred.addCallback(lambda _: data)
    return deferred
```

## To Do

* Add more parts of Google analytics and more analytics providers.
